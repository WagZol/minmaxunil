package test;

import java.util.Scanner;

public class test {
    static int max;
    static int min;
    public static String minMaxUntil(){
        Scanner scan=new Scanner(System.in);
        int szam;
        int ciklusSzamlalo=0;
        while (true){
            System.out.println("Írj be egy számot, vagy írd be a -9999999-et a kilépéshez!:");
            szam=scan.nextInt();
            if (szam==-9999999){
                break;
            }
            if(ciklusSzamlalo==0){
                max=szam;
                min=szam;
            }
            else{
                if (max<szam){
                    max=szam;
                }
                if (min>szam){
                    min=szam;
                }
            }
            ciklusSzamlalo++;
        }
        return ciklusSzamlalo>0?"Az általad beírt sorozat maximuma: "+max+", minimuma: "+min:"Nem írtál be számot a -9999999-en kívül, így nincs minimum, maximum!";
    }

    public static void main(String[] args) {
        System.out.println(minMaxUntil());
    }
}
